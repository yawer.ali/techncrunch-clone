import useGet from 'hooks/useGet'
import { useEffect, useState } from 'react'
import moment from 'moment'
import {
  LatestNewsContent,
  LatestNewsImg,
  LatestNewsImgWrapper,
  LatestNewsTitle,
  LatestNewsTitleAuthDateWrapper,
  LatestNewsWrapper,
  NewsAuthor,
  NewsDate,
  LoadMoreWrapper,
  LoadMoreButton,
} from 'styles/views/topNews/index'

import { useNavigate } from 'react-router-dom'

import styled from 'styled-components'

import Loader from 'components/Loader'

const StartUps = () => {
  const [page, setPage] = useState(1)
  const [perPagePost, setPerPagePost] = useState(20)
  const {
    refetch: fetchDetails,
    data,
    isLoading,
    error,
    isFetching,
  } = useGet('startups-length', `posts?category_slug=startups&per_page=${perPagePost}&page=${page}`)
  //   console.log('Startups data', data)

  const loadMoreData = () => {
    if (page == 5) {
      setPage(1)
      setPerPagePost(perPagePost - 80)
    } else {
      setPage(page + 1)
      setPerPagePost(perPagePost + 20)
    }
  }

  const navigate = useNavigate()

  useEffect(() => {
    fetchDetails()
  }, [page])

  if (error) return <h1>Error...</h1>
  const handleclickItem = (item: any) => {
    navigate('/singlepost', { state: item })
  }

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <MainStartUpWrapper>
          <StartUpHeading>Startups</StartUpHeading>
          <StartUpTitle>
            zTech startup coverage that breaks down the funding, growth, and long-term trajectory of companies across
            every stage and industry, including climate, crypto, fintech, SaaS, transportation, and consumer tech.
          </StartUpTitle>

          {data?.map((item: any, index: number) => {
            const AuthorName = 'parsely-author'
            const content = item.excerpt.rendered
            const contentData = content.slice(3, content.length - 15)
            const Title = item.title.rendered

            return (
              <LatestNewsWrapper key={index} onClick={() => handleclickItem(item)}>
                <LatestNewsTitleAuthDateWrapper>
                  <LatestNewsTitle dangerouslySetInnerHTML={{ __html: Title }}></LatestNewsTitle>
                  <NewsAuthor>{item.parselyMeta[AuthorName]}</NewsAuthor>
                  <NewsDate>{moment(item.date).format('LLL')}</NewsDate>
                </LatestNewsTitleAuthDateWrapper>
                <LatestNewsContent dangerouslySetInnerHTML={{ __html: contentData }}></LatestNewsContent>
                <LatestNewsImgWrapper>
                  <LatestNewsImg src={item.jetpack_featured_media_url} />
                </LatestNewsImgWrapper>
              </LatestNewsWrapper>
            )
          })}

          <LoadMoreWrapper>
            <LoadMoreButton onClick={loadMoreData}>{isFetching ? 'loading' : 'Load More'}</LoadMoreButton>
          </LoadMoreWrapper>
        </MainStartUpWrapper>
      )}
    </>
  )
}

export default StartUps

// styles
export const MainStartUpWrapper = styled.div`
  margin-left: 15vw;
  /* border: 2px solid green; */
`
export const StartUpHeading = styled.div`
  font-size: 5vw;
  font-weight: 800;
`
export const StartUpTitle = styled.div`
  font-size: 1vw;
  font-weight: 400;
  width: 45vw;
  color: black;
`
