import useGet from 'hooks/useGet'
import { useEffect, useState } from 'react'
import moment from 'moment'
import {
  LatestNewsContent,
  LatestNewsHeading,
  LatestNewsImg,
  LatestNewsImgWrapper,
  LatestNewsTitle,
  LatestNewsTitleAuthDateWrapper,
  LatestNewsWrapper,
  MainNewsWrapper,
  NewsAuthor,
  NewsDate,
  LoadMoreWrapper,
  LoadMoreButton,
} from 'styles/views/topNews'
// import TrendingNews from 'views/trendingnews'
import Loader from 'components/Loader'
import { useNavigate } from 'react-router-dom'

const TopNews = () => {
  const [page, setPage] = useState(1)
  const [perPagePost, setPerPagePost] = useState(10)
  const {
    refetch: fetchDetails,
    data,
    isLoading,
    error,
    isFetching,
  } = useGet('posts-length', `posts?per_page=${perPagePost}&page=${page}`)
  // console.log('topnewsdata', data)

  const loadMoreData = () => {
    if (page == 10) {
      setPage(1)
      setPerPagePost(perPagePost - 90)
    } else {
      setPage(page + 1)
      setPerPagePost(perPagePost + 10)
    }
  }

  const navigate = useNavigate()

  useEffect(() => {
    fetchDetails()
  }, [page])

  if (isLoading) return <Loader />

  if (error) return <h1>Error...</h1>

  return (
    <MainNewsWrapper>
      {/* <TrendingNews /> */}
      <LatestNewsHeading>The Latest</LatestNewsHeading>
      {data?.map((item: any, index: number) => {
        const AuthorName = 'parsely-author'
        const content = item.excerpt.rendered
        const contentData = content.slice(3, content.length - 15)
        const Title = item.title.rendered
        const handleclickItem = (item: any) => {
          navigate('/singlepost', { state: item })
        }

        return (
          <LatestNewsWrapper key={index} onClick={() => handleclickItem(item)}>
            <LatestNewsTitleAuthDateWrapper>
              <LatestNewsTitle dangerouslySetInnerHTML={{ __html: Title }}></LatestNewsTitle>
              <NewsAuthor>{item.parselyMeta[AuthorName]}</NewsAuthor>
              <NewsDate>{moment(item.date).format('LLL')}</NewsDate>
            </LatestNewsTitleAuthDateWrapper>
            <LatestNewsContent dangerouslySetInnerHTML={{ __html: contentData }}></LatestNewsContent>
            <LatestNewsImgWrapper>
              <LatestNewsImg src={item.jetpack_featured_media_url} />
            </LatestNewsImgWrapper>
          </LatestNewsWrapper>
        )
      })}
      <LoadMoreWrapper>
        <LoadMoreButton onClick={loadMoreData}>{isFetching ? 'loading' : 'Load More'}</LoadMoreButton>
      </LoadMoreWrapper>
    </MainNewsWrapper>
  )
}

export default TopNews
