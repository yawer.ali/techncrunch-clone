import React, { useState } from 'react'
import { HeaderMainWrapper, LogoWrapper, Logo, MenuBar } from 'styles/views/header'
import logo from 'assets/images/logo.png'
import { GiHamburgerMenu } from 'react-icons/gi'
import MyModal from 'components/Modal/index'
import { useNavigate } from 'react-router-dom'

const Header = () => {
  const [modal, setModal] = useState(false)
  const handleclick = () => {
    setModal(true)
  }
  const navigate = useNavigate()
  const handlecLogoClick = () => {
    navigate('/')
  }

  return (
    <HeaderMainWrapper>
      <LogoWrapper>
        <Logo src={logo} onClick={handlecLogoClick} />
      </LogoWrapper>
      <MenuBar>
        <GiHamburgerMenu onClick={handleclick} />
        {modal && <MyModal isOpen={modal} isClose={() => setModal(false)} />}
      </MenuBar>
    </HeaderMainWrapper>
  )
}

export default Header
