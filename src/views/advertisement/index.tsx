/* eslint-disable react/no-unescaped-entities */
import useGet from 'hooks/useGet'
import { useEffect, useState } from 'react'
import {
  AdsMainWrapper,
  AdsWrapper,
  ButtonsWrapper,
  Content,
  EventsWrapper,
  MainButton,
  Title,
  TitleAndContentWrapper,
  Events,
  EventDate,
  EventsAndDatesWrapper,
  EventsDateAndName,
  Line,
  MainWrapper,
} from 'styles/views/advertisement'
import Loader from 'components/Loader'

const Advertisement = () => {
  const [value, setValue] = useState(0)
  const handleClick = (value: number) => {
    setValue(value)
  }
  const {
    refetch: fetchDetails,
    data,
    isLoading,
  } = useGet('ads-length', `https://techcrunch.com//wp-json/wp/v2/pages?_embed=true&slug=advertisement-events-calendar`)
  // console.log('ads data', data)

  useEffect(() => {
    fetchDetails()
  }, [])

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <MainWrapper>
          {data?.map((ads: any) => {
            return (
              <AdsMainWrapper key={ads.id}>
                <AdsWrapper>
                  <TitleAndContentWrapper>
                    <Title>{ads.opportunities.title}</Title>
                    <Title>{ads.title.rendered}</Title>
                    <Content dangerouslySetInnerHTML={{ __html: ads.excerpt.rendered }} />
                    <ButtonsWrapper>
                      <MainButton>Tc Brand Studio</MainButton>
                      <MainButton>Advertising Updates</MainButton>
                      <MainButton>Get In Touch</MainButton>
                    </ButtonsWrapper>
                  </TitleAndContentWrapper>
                </AdsWrapper>
                <EventsWrapper>
                  {ads.opportunities.map((events: any, index: number) => {
                    return (
                      <Events key={events.id} onClick={() => handleClick(index)}>
                        {index == 3 ? ' ' : events.title}
                      </Events>
                    )
                  })}
                </EventsWrapper>
                {ads?.opportunities[value]?.sections[0]?.items?.map((event: any) => {
                  return value == 3 ? (
                    <h1>hello</h1>
                  ) : (
                    <EventsAndDatesWrapper key={event.id}>
                      <EventDate>{ads?.opportunities[value]?.sections[0]?.title}</EventDate>
                      <Line />
                      <EventsDateAndName dangerouslySetInnerHTML={{ __html: event?.content }} />
                    </EventsAndDatesWrapper>
                  )
                })}
              </AdsMainWrapper>
            )
          })}
        </MainWrapper>
      )}
    </>
  )
}

export default Advertisement
