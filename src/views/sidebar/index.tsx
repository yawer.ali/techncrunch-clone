import { useState } from 'react'
import {
  CloseSearchText,
  JoinText,
  JoinTextWrapper,
  Link,
  LoginText,
  LoginTextMainWrapper,
  LoginWrapper,
  Logo,
  LogoWrapper,
  MenuLinks,
  MenuLinksWrapper,
  SearchBoxWrapper,
  SearchIcontWrapper,
  SearchText,
  SearchWrapper,
  SearchTextWrapper,
  SideBarMainWrapper,
  SearchInputWrapper,
  SearchTextInput,
  CloseSearchWrapper,
  CloseWrapper,
  SearchIconWrapper,
  SIcontWrapper,
} from 'styles/views/sideBar'
import logo from 'assets/images/logo.png'
import { IoIosSearch } from 'react-icons/io/index'
import { ImCross } from 'react-icons/im/index'
import { useNavigate } from 'react-router-dom'
import SideBarModal from 'components/SideBarModal'

const Sidebar = () => {
  const [open, setOpen] = useState(false)
  const [isopen, setIsOpen] = useState(false)
  const [val, setVal] = useState('')

  const switchsearch = () => {
    open ? setOpen(false) : setOpen(true)
  }

  const handlecMoreClick = () => {
    isopen ? setIsOpen(false) : setIsOpen(true)
  }

  const navigate = useNavigate()

  const handleClick = () => {
    navigate('/login', { state: val })
  }
  const handleclickItem = () => {
    navigate('/news', { state: val })
    open ? setOpen(false) : setOpen(true)
  }
  const handlecStartUpClick = () => {
    navigate('/startups')
  }

  const handlecTechCrunchClick = () => {
    navigate('/techcrunchphus')
  }
  const handlecVentureClick = () => {
    navigate('/venture')
  }

  const handlecSecurityClick = () => {
    navigate('/security')
  }

  const handlecCryptoClick = () => {
    navigate('/crypto')
  }

  const handlecAppsClick = () => {
    navigate('/apps')
  }

  const handleEventsClick = () => {
    navigate('/events')
  }

  const handleAdsClick = () => {
    navigate('/advertisement')
  }

  const handlecLogoClick = () => {
    navigate('/')
  }

  return (
    <SideBarMainWrapper>
      <MenuLinksWrapper>
        <LogoWrapper>
          <Logo src={logo} onClick={handlecLogoClick} />
        </LogoWrapper>
        <LoginTextMainWrapper>
          <JoinTextWrapper>
            <JoinText>Join TechCrunch+</JoinText>
          </JoinTextWrapper>
          <LoginWrapper>
            <LoginText onClick={handleClick}>Login</LoginText>
          </LoginWrapper>
        </LoginTextMainWrapper>
        <MenuLinks>
          <SearchTextWrapper>
            {open ? (
              <CloseSearchWrapper>
                <CloseWrapper>
                  <CloseSearchText onClick={switchsearch}>
                    Close Search <ImCross className="crossIcon" />
                  </CloseSearchText>
                </CloseWrapper>
                <SearchIconWrapper>
                  <SearchInputWrapper>
                    <SearchTextInput
                      type="text"
                      value={val}
                      placeholder="Search TechCrunch"
                      onChange={(e: any) => {
                        setVal(e.target.value)
                      }}
                    />
                  </SearchInputWrapper>
                  <SIcontWrapper>
                    <IoIosSearch className="searchIcon" onClick={handleclickItem} />
                  </SIcontWrapper>
                </SearchIconWrapper>
              </CloseSearchWrapper>
            ) : (
              <SearchWrapper>
                <SearchText onClick={switchsearch}>Search</SearchText>
                <SearchIcontWrapper>
                  <IoIosSearch />
                </SearchIcontWrapper>
              </SearchWrapper>
            )}
          </SearchTextWrapper>
          <Link onClick={handlecTechCrunchClick}>TechCrunch+</Link>
          <Link onClick={handlecStartUpClick}>Startups</Link>
          <Link onClick={handlecVentureClick}>Venture</Link>
          <Link onClick={handlecSecurityClick}>Security</Link>
          <Link onClick={handlecCryptoClick}>Crypto</Link>
          <Link onClick={handlecAppsClick}>Apps</Link>
          <Link onClick={handleEventsClick}>Events</Link>
          <Link onClick={handleAdsClick}>Advertise</Link>
          {isopen ? (
            <SideBarModal isOpen={isopen} isClose={() => setIsOpen(false)} />
          ) : (
            <Link onClick={handlecMoreClick}>More</Link>
          )}
        </MenuLinks>
      </MenuLinksWrapper>
    </SideBarMainWrapper>
  )
}

export default Sidebar
