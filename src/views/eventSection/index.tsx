/* eslint-disable react/no-unescaped-entities */
import useGet from 'hooks/useGet'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import {
  BuyButton,
  BuyButtonWrapper,
  EventsButtonsWrapper,
  EventsCityAndCountryWrapper,
  EventsDateWrapper,
  EventsHeading,
  EventsTitleWrapper,
  EventsWrapper,
  MainEventsWrapper,
  PastEventsContent,
  PastEventsDate,
  PastEventsHeadng,
  PastEventsMainWrapper,
  PastEventsWrapper,
  SponsorButton,
  SponsorButtonWrapper,
} from 'styles/views/eventSection'
import Loader from 'components/Loader'

const Events = () => {
  const { refetch: fetchDetails, data, isLoading } = useGet('events-length', `tc_events?&per_page=9`)
  // console.log('Events data', data)

  useEffect(() => {
    fetchDetails()
  }, [])

  const navigate = useNavigate()
  const handleclickItem = (event: any) => {
    navigate('/singlevent', { state: event })
  }

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <MainEventsWrapper>
          <EventsHeading>Where We'll Be Next</EventsHeading>

          {data?.map((pastEvent: any) => {
            return (
              <EventsWrapper key={pastEvent.id} onClick={() => handleclickItem(pastEvent)}>
                <EventsDateWrapper>{pastEvent.date}</EventsDateWrapper>
                <EventsTitleWrapper>{pastEvent.title.rendered}</EventsTitleWrapper>
                <EventsCityAndCountryWrapper>
                  {pastEvent?.venue?.city}
                  {pastEvent?.venue?.country}
                </EventsCityAndCountryWrapper>
                <EventsButtonsWrapper>
                  <BuyButtonWrapper>
                    <BuyButton>Buy Tickets</BuyButton>
                  </BuyButtonWrapper>

                  <SponsorButtonWrapper>
                    <SponsorButton>Be a Sponsor</SponsorButton>
                  </SponsorButtonWrapper>
                </EventsButtonsWrapper>
              </EventsWrapper>
            )
          })}
        </MainEventsWrapper>
      )}
      <PastEventsMainWrapper>
        <PastEventsHeadng>Past Events</PastEventsHeadng>
        {data?.map((pastEvent: any) => {
          return (
            <PastEventsWrapper key={pastEvent.id} onClick={() => handleclickItem(pastEvent)}>
              <PastEventsContent>{pastEvent.title.rendered}</PastEventsContent>
              <PastEventsDate>{pastEvent.date}</PastEventsDate>
            </PastEventsWrapper>
          )
        })}
      </PastEventsMainWrapper>
    </>
  )
}

export default Events
