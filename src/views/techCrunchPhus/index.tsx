import useGet from 'hooks/useGet'
import { useEffect, useState } from 'react'
import moment from 'moment'
import {
  LatestNewsContent,
  LatestNewsHeading,
  LatestNewsImg,
  LatestNewsImgWrapper,
  LatestNewsTitle,
  LatestNewsTitleAuthDateWrapper,
  LatestNewsWrapper,
  MainNewsWrapper,
  NewsAuthor,
  NewsDate,
  LoadMoreWrapper,
  LoadMoreButton,
  TopNewsWrapper,
  TopNewsImgWrapper,
  TopNewsTitle,
  TopNewsImg,
  TopNewsMainWrapper,
  TechHeadingWrapper,
  TechCrunchWrapper,
  HelpingStartupsWrapper,
  FaqWrapper,
  Plus,
} from 'styles/views/techCrunch'
// import TrendingNews from 'views/trendingnews'
import Loader from 'components/Loader'
import { useNavigate } from 'react-router-dom'

const TechCrunchPhus = () => {
  const [page, setPage] = useState(1)
  const [perPagePost, setPerPagePost] = useState(10)
  const {
    refetch: fetchDetails,
    data,
    isLoading,
    error,
    isFetching,
  } = useGet('tech-length', `posts?&page=${page}&per_page=${perPagePost}&_embed=true&_envelope=true`)
  //   console.log('TECHCRUNCH+', data)

  const loadMoreData = () => {
    if (page == 10) {
      setPage(1)
      setPerPagePost(perPagePost - 90)
    } else {
      setPage(page + 1)
      setPerPagePost(perPagePost + 10)
    }
  }

  const navigate = useNavigate()

  useEffect(() => {
    fetchDetails()
  }, [page])

  if (error) return <h1>Error...</h1>
  // console.log('topnews', data)

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <MainNewsWrapper>
          <TechHeadingWrapper>
            <TechCrunchWrapper>
              TECHCRUNCH<Plus>+</Plus>
            </TechCrunchWrapper>
            <HelpingStartupsWrapper>HELPING FOUNDERS AND STARTUP TEAMS GET AHEAD</HelpingStartupsWrapper>
            <FaqWrapper>FAQ</FaqWrapper>
          </TechHeadingWrapper>
          {/* <TrendingNews /> */}
          <TopNewsMainWrapper>
            {data?.body.slice(0, 9).map((news: any) => {
              return (
                <TopNewsWrapper key={news.id}>
                  <TopNewsImgWrapper>
                    <TopNewsImg src={news.jetpack_featured_media_url} alt="img" />
                  </TopNewsImgWrapper>
                  <TopNewsTitle>{news.title.rendered}</TopNewsTitle>
                </TopNewsWrapper>
              )
            })}
          </TopNewsMainWrapper>
          <LatestNewsHeading>The Latest TechCrunch+ Articles</LatestNewsHeading>
          {data?.body?.map((item: any, index: number) => {
            const AuthorName = 'parsely-author'
            const content = item.excerpt.rendered
            const contentData = content.slice(3, content.length - 15)
            const Title = item.title.rendered
            const handleclickItem = (item: any) => {
              navigate('/singlepost', { state: item })
            }

            return (
              <LatestNewsWrapper key={index} onClick={() => handleclickItem(item)}>
                <LatestNewsImgWrapper>
                  <LatestNewsImg src={item.jetpack_featured_media_url} />
                </LatestNewsImgWrapper>
                <LatestNewsTitleAuthDateWrapper>
                  <LatestNewsTitle dangerouslySetInnerHTML={{ __html: Title }}></LatestNewsTitle>
                  <LatestNewsContent dangerouslySetInnerHTML={{ __html: contentData }}></LatestNewsContent>
                  <NewsAuthor>{item.parselyMeta[AuthorName]}</NewsAuthor>
                  <NewsDate>{moment(item.date).format('LLL')}</NewsDate>
                </LatestNewsTitleAuthDateWrapper>
              </LatestNewsWrapper>
            )
          })}
          <LoadMoreWrapper>
            <LoadMoreButton onClick={loadMoreData}>{isFetching ? 'loading' : 'Load More'}</LoadMoreButton>
          </LoadMoreWrapper>
        </MainNewsWrapper>
      )}
    </>
  )
}

export default TechCrunchPhus
