import { useLocation } from 'react-router-dom'
import {
  ScrollIndicatorWrapper,
  SinglePagePostWrapper,
  SinglePostAuthor,
  SinglePostAuthorEmail,
  SinglePostAuthWrapper,
  SinglePostContent,
  SinglePostDate,
  SinglePostImg,
  SinglePostImgWrapper,
  SinglePostTitle,
  SinglePostTitleAndAuthWrapper,
  SinglePostWrapper,
} from 'styles/components/SinglePost'

const SinglePost = () => {
  const { state } = useLocation()
  const AuthorName = 'parsely-author'
  const Title = state.title.rendered
  const content = state.content.rendered
  return (
    <>
      <SinglePagePostWrapper>
        <SinglePostWrapper>
          <SinglePostTitleAndAuthWrapper>
            <SinglePostTitle dangerouslySetInnerHTML={{ __html: Title }} />
            <SinglePostAuthWrapper>
              <SinglePostAuthor>{state.parselyMeta[AuthorName]}</SinglePostAuthor>
              <SinglePostAuthorEmail>@gmail.com</SinglePostAuthorEmail>
              <SinglePostDate>{state.date}</SinglePostDate>
            </SinglePostAuthWrapper>
          </SinglePostTitleAndAuthWrapper>
          <SinglePostImgWrapper>
            <SinglePostImg src={state.jetpack_featured_media_url} />
          </SinglePostImgWrapper>
          <SinglePostContent dangerouslySetInnerHTML={{ __html: content }} />
        </SinglePostWrapper>
        <ScrollIndicatorWrapper></ScrollIndicatorWrapper>
      </SinglePagePostWrapper>
    </>
  )
}

export default SinglePost
