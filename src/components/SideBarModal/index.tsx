import Typography from '@mui/material/Typography'
import Modal from '@mui/material/Modal'
import Box from '@mui/material/Box'
import styled from 'styled-components'
import { VscClose } from 'react-icons/vsc'
import logo from 'assets/images/logo.png'

interface ModalProps {
  isOpen: boolean
  isClose: () => void
}

const style = {
  position: 'absolute',
  top: '32%',
  left: '6%',
  transform: 'translate(-50%, -50%)',
  width: '17vw',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 6,
}

const SideBarModal = ({ isOpen, isClose }: ModalProps) => {
  return (
    <Modal open={isOpen} onClose={isClose}>
      <Box sx={style}>
        <Typography>
          <ModalWrapper>
            <ModalLinks>
              <MainLogoAndIconWrapper>
                <LogoWrapper>
                  <Logo src={logo} />
                </LogoWrapper>
                <CloseButtonWrapper>
                  <VscClose onClick={isClose} />
                </CloseButtonWrapper>
              </MainLogoAndIconWrapper>

              <JoinTextWrapper>
                <JoinText>More TechCrunch</JoinText>
              </JoinTextWrapper>

              <Link>Startup Battlefield</Link>
              <Link>Fintech</Link>
              <Link>Hardware</Link>
              <Link>Transportation</Link>
              <Link>Media & Entertainment</Link>
              <Link>Newsletters</Link>
              <Link>Podcasts</Link>
              <Link>Partner Content</Link>
              <Link>Crunchboard Jobs</Link>
              <Link>Contact Us</Link>
            </ModalLinks>
          </ModalWrapper>
        </Typography>
      </Box>
    </Modal>
  )
}

export default SideBarModal

// styles
export const ModalWrapper = styled.div`
  color: #777;
`

export const MainLogoAndIconWrapper = styled.div`
  width: 12vw;
  padding-top: 5vw;
  display: flex;
`
export const LogoWrapper = styled.div`
  width: 7vw;
  height: 7vh;
`
export const Logo = styled.img`
  width: 4.5vw;
  height: 4.5vh;
`

export const CloseButtonWrapper = styled.div`
  font-size: 2vw;
  padding-left: 5vw;
  :hover {
    cursor: pointer;
    color: rgb(0, 208, 130);
  }
`

export const JoinTextWrapper = styled.div`
  width: 13vw;
  padding: 1vw 1vw;
`
export const JoinText = styled.div`
  font-size: 0.9vw;
  font-weight: 900;
  color: #000;
`
export const MenuLinksWrapper = styled.div`
  width: 20vw;
`

export const ModalLinks = styled.div`
  width: 14vw;
  padding: 0.4vw;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  gap: 0.7vw;
  height: 130.313vh;
`
export const Link = styled.div`
  width: 13vw;
  font-size: 1vw;
  font-weight: 400;
  padding: 0.1vw 1vw;
  :hover {
    cursor: pointer;
    color: rgb(0, 208, 130);
  }
`
