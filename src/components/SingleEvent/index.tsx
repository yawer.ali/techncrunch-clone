import { useLocation } from 'react-router-dom'
import {
  City,
  CityAndContentWrapper,
  CityAndDateWrapper,
  Content,
  Date,
  ImageWrapper,
  InfoDescription,
  InfoTitle,
  InfoWrapper,
  Line,
  MediaImg,
  NewsDescription,
  NewsMainWrapper,
  NewsTitle,
  Policy,
  SingleEventMainWrapper,
  Title,
  TitleAndDateWrapper,
  TitleAndDescriptionWrapper,
} from 'styles/components/SingleEvent'

const SingleEvent = () => {
  const { state } = useLocation()

  return (
    <>
      <SingleEventMainWrapper>
        <TitleAndDateWrapper>
          <Title>{state.title.rendered}</Title>
          <CityAndContentWrapper>
            <CityAndDateWrapper>
              <City>{state?.venue?.city}</City>
              <Date>{state.date}</Date>
            </CityAndDateWrapper>
            <Content>{state.description}</Content>
          </CityAndContentWrapper>
        </TitleAndDateWrapper>

        <InfoWrapper>
          <InfoTitle>{state?.blocks[0]?.title}</InfoTitle>
          <Line />
          <InfoDescription>{state?.description}</InfoDescription>
        </InfoWrapper>
        {state?.blocks[0]?.textThumbnail?.map((event: any, index: number) => {
          return (
            <NewsMainWrapper key={index} a={index % 2 === 0 && 'row-reverse'}>
              <TitleAndDescriptionWrapper>
                <NewsTitle>{event?.title}</NewsTitle>
                <NewsDescription>{event?.description}</NewsDescription>
              </TitleAndDescriptionWrapper>
              <ImageWrapper>
                <MediaImg src={event?.imageURL} alt="Media Image" />
              </ImageWrapper>
            </NewsMainWrapper>
          )
        })}
        <Policy dangerouslySetInnerHTML={{ __html: state.excerpt.rendered }} />
      </SingleEventMainWrapper>
    </>
  )
}

export default SingleEvent
