import Typography from '@mui/material/Typography'
import Modal from '@mui/material/Modal'
import Box from '@mui/material/Box'
import styled from 'styled-components'
import { VscClose } from 'react-icons/vsc'
import { IoIosSearch } from 'react-icons/io/index'

interface ModalProps {
  isOpen: boolean
  isClose: () => void
}

const style = {
  position: 'absolute',
  top: '35%',
  left: '90%',
  transform: 'translate(-50%, -50%)',
  width: '40vw',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 3,
}

const MyModal = ({ isOpen, isClose }: ModalProps) => {
  return (
    <Modal open={isOpen} onClose={isClose}>
      <Box sx={style}>
        <Typography>
          <ModalWrapper>
            <ModalLinks>
              <CloseButtonWrapper>
                <VscClose onClick={isClose} />
              </CloseButtonWrapper>
              <LoginTextMainWrapper>
                <JoinTextWrapper>
                  <JoinText>Join TechCrunch+</JoinText>
                </JoinTextWrapper>
                <LoginWrapper>
                  <LoginText>Login</LoginText>
                </LoginWrapper>
              </LoginTextMainWrapper>

              <SearchTextWrapper>
                <SearchText>Search</SearchText>
                <SearchIcontWrapper>
                  <IoIosSearch />
                </SearchIcontWrapper>
              </SearchTextWrapper>

              <Link>TechCrunch+</Link>
              <Link>Startups</Link>
              <Link>Venture</Link>
              <Link>Security</Link>
              <Link>Crypto</Link>
              <Link>Apps</Link>
              <Link>Events</Link>
              <Link>Advertise</Link>
              <Link>More</Link>
            </ModalLinks>
          </ModalWrapper>
        </Typography>
      </Box>
    </Modal>
  )
}

export default MyModal

// styles
export const ModalWrapper = styled.div`
  font-size: 2.3vw;
  font-weight: 300;
  color: #777;
`
export const ModalLinks = styled.div`
  width: 14vw;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  height: 130.313vh;
`
export const CloseButtonWrapper = styled.div`
  padding-left: 35vw;
  padding-top: 5vw;
  font-size: 4vw;
  :hover {
    cursor: pointer;
    color: rgb(0, 208, 130);
  }
`
export const LoginTextMainWrapper = styled.div`
  width: 15vw;
`
export const JoinTextWrapper = styled.div`
  width: 22vw;
`
export const JoinText = styled.div`
  font-size: 2.4vw;
  font-weight: 900;
  color: #fdc006;
  :hover {
    cursor: pointer;
    color: rgb(255, 203, 112);
  }
`

export const LoginWrapper = styled.div`
  :hover {
    cursor: pointer;
    color: green;
  }
`
export const LoginText = styled.div`
  :hover {
    color: rgb(0, 208, 130);
  }
`

export const SearchTextWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 2vw;
  :hover {
    cursor: pointer;
  }
`
export const SearchText = styled.div`
  width: 9vw;
`
export const SearchIcontWrapper = styled.div`
  width: 6vw;
  padding-top: 1vw;
`

export const Link = styled.div`
  width: 15vw;
  padding-bottom: 0.5vw;
  :hover {
    cursor: pointer;
    color: rgb(0, 208, 130);
  }
`
