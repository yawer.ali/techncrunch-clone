import styled from 'styled-components'

export const SideBarMainWrapper = styled.div`
  font-size: 25px;
  font-weight: 200;
  color: #777;
  position: fixed;
  /* border: 2px solid red; */
  @media only screen and (max-width: 1024px) {
    display: none;
  }
`

export const LogoWrapper = styled.div`
  width: 10vw;
  margin-top: 1vh;
`

export const Logo = styled.img`
  width: 6vw;
  padding: 1vw;
  margin-left: 0.5vw;
  :hover {
    cursor: pointer;
  }
`

export const LoginTextMainWrapper = styled.div`
  width: 13vw;
  /* margin-left: 0.9vw; */
`
export const JoinTextWrapper = styled.div`
  width: 13vw;
  display: flex;
  justify-content: center;
  align-items: center;
`
export const JoinText = styled.div`
  width: 10vw;
  font-size: 1vw;
  font-weight: 900;
  color: #fdc006;
  :hover {
    cursor: pointer;
    color: green;
  }
`
export const LoginWrapper = styled.div`
  width: 10vw;
  display: flex;
  justify-content: center;
  align-items: center;
  :hover {
    cursor: pointer;
    color: green;
  }
`
export const LoginText = styled.div`
  width: 7vw;
  :hover {
    color: rgb(0, 208, 130);
  }
`

export const SearchTextWrapper = styled.div`
  width: 8vw;
  /* border: 2px solid red; */
  height: 6vh;
  margin-top: 2.5vh;

  :hover {
    cursor: pointer;
  }
`

export const SearchWrapper = styled.div`
  width: 8vw;
  display: flex;
  align-items: center;
  justify-content: center;
`
export const SearchText = styled.div`
  width: 6vw;
  /* font-size: 1vw; */
`
export const SearchIcontWrapper = styled.div`
  width: 6vw;
  margin-top: 1vh;
`

export const CloseSearchWrapper = styled.div`
  width: 15vw;
  display: flex;
  /* border: 1px solid red; */
  /* border: 2px solid blue; */
`

export const CloseSearchText = styled.div`
  background-color: black;
  padding: 14px 6px;
  width: 12vw;
  font-size: 1.1vw;
  font-weight: 400;
  color: white;
  .crossIcon {
    margin-left: 1.5vw;
  }
`

export const CloseWrapper = styled.div``
export const SIcontWrapper = styled.div``
export const SearchIconWrapper = styled.div`
  border: 1px solid green;
  display: flex;
  background-color: white;
  color: #000;
  .searchIcon {
    color: green;
    margin-top: 10px;
  }
`
export const SearchInputWrapper = styled.div``
export const SearchTextInput = styled.input`
  border: none;
  &:focus {
    outline: none;
  }
`

// export const DropDown = styled.div`
//   background-color: white;
//   border: 1px solid green;
// `

// export const DropDownRow = styled.div`
//   cursor: pointer;
// `

export const MenuLinksWrapper = styled.div`
  width: 10vw;
`

export const MenuLinks = styled.div`
  width: 10vw;
  margin-left: 9px;
  padding: 0.5vw;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`
export const Link = styled.div`
  width: 8vw;
  padding-bottom: 1px;
  :hover {
    cursor: pointer;
    color: green;
  }
`
