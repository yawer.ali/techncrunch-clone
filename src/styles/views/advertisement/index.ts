import styled from 'styled-components'

export const MainWrapper = styled.div`
  margin-left: 15vw;
`

export const AdsMainWrapper = styled.div``
export const AdsWrapper = styled.div`
  background-image: linear-gradient(to right, #434343 0%, black 100%);
  /* background-color: black; */
  /* background-image: url('https://techcrunch.com/wp-content/uploads/2019/12/48381336202_cf6b0389f0_k.jpg?w=1024'); */
  padding: 1vw;
`

export const TitleAndContentWrapper = styled.div`
  color: #fff;
  padding: 1vw;
`
export const Title = styled.div`
  font-size: 3.8vw;
  font-weight: 800;
  width: 65vw;
`
export const Content = styled.div`
  font-size: 2vw;
  font-weight: 400;
  width: 55vw;
`
export const ButtonsWrapper = styled.div`
  display: flex;
  gap: 5vw;
  padding: 1vw 0;
`
export const MainButton = styled.button`
  background-color: #fff;
  color: black;
  font-size: 1vw;
  font-size: 14px;
  padding: 1.2vw 2vw;
  border: 0;
  font-weight: 700;
  cursor: pointer;
`

export const EventsWrapper = styled.div`
  display: flex;
  gap: 2vw;
  padding: 0.6vw 0;
  width: 31vw;
  border-bottom: 3px solid #f1f1f1;
`
export const Events = styled.div`
  color: #00a562;
  font-size: 1vw;
  font-weight: 700;
  :hover {
    cursor: pointer;
    border-bottom: 1px solid #14c435;
    color: black;
  }
`

export const EventsAndDatesWrapper = styled.div``
export const EventDate = styled.div`
  font-size: 3vw;
  font-weight: 800;
`
export const EventsDateAndName = styled.div`
  font-size: 1.2vw;
`
export const Line = styled.div`
  height: 3px;
  width: 60px;
  margin: 20px 0;
  background-color: #14c435;
`
