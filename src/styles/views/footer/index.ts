import styled from 'styled-components'

export const FooterWrapper = styled.div`
  width: 80vw;
  margin-left: 15vw;
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  color: #777;
  font-size: 0.9vw;
  border-top: 2px solid #f1f1f1;
`
export const AboutandLinksWrapper = styled.div`
  width: 12vw;
`
export const AboutWrapper = styled.div`
  width: 10vw;
`
export const AboutHeading = styled.div`
  width: 10vw;
  font-weight: 650;
  color: black;
`

export const AboutLinks = styled.div`
  width: 9vw;
  font-weight: 200;
  padding: 0.2vw;
  &:hover {
    cursor: pointer;
    border-bottom: 1px solid #d3d3d3;
  }
`

export const LegalandLinksWrapper = styled.div`
  width: 12vw;
  list-style: none;
`

export const LegalWrapper = styled.div`
  width: 10vw;
`
export const LegalHeading = styled.div`
  width: 10vw;
  font-size: 1vw;
  color: black;
  font-weight: 650;
`

export const LegalLinks = styled.div`
  width: 10vw;
  padding: 0.2vw;
  font-weight: 200;
  &:hover {
    cursor: pointer;
    border-bottom: 1px solid #d3d3d3;
  }
`
export const TechandLinksWrapper = styled.div`
  width: 12vw;
`

export const TechWrapper = styled.div`
  width: 13vw;
`
export const TechHeading = styled.div`
  width: 13vw;
  color: black;
  font-weight: 650;
`

export const TechLinks = styled.div`
  width: 10vw;
  padding: 0.2vw;
  font-weight: 200;
  &:hover {
    cursor: pointer;
    border-bottom: 1px solid #d3d3d3;
  }
  /* border: 2px solid red; */
`

export const SocialIconsTextWrapper = styled.div`
  width: 20vw;
  margin-left: 10vw;
  .icons {
    font-size: 1.5vw;
  }
  .icons:hover {
    color: red;
  }
  /* border: 2px solid red; */
`

export const IconsTextWrapper = styled.div`
  width: 10vw;
  display: flex;
`

export const FbIconTextWrapper = styled.div`
  width: 10vw;
`
export const FbIconText = styled.div`
  width: 10vw;
`
export const SocialText = styled.span`
  font-size: 1vw;
  /* margin: 0.7vw; */
  padding: 1vw;
`
export const TwIconsTextWrapper = styled.div`
  width: 10vw;
`
export const TwIconsText = styled.div`
  width: 10vw;
`
export const BottomTextWrapper = styled.div`
  width: 50vw;
  font-weight: 200;
  color: #777;
`
