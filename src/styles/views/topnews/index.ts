import styled from 'styled-components'

export const MainNewsWrapper = styled.div`
  margin-left: 15vw;
`

export const LatestNewsHeading = styled.h2`
  font-size: 1.5vw;
`

export const LatestNewsWrapper = styled.div`
  width: 68vw;
  display: flex;
  margin-top: 2vh;
  gap: 1vw;
  padding: 0.5vw 0;
  border-top: 2px solid #f1f1f1;
  :hover {
    cursor: pointer;
    opacity: 0.6;
  }
`

export const LatestNewsTitleAuthDateWrapper = styled.div`
  width: 20vw;
  @media only screen and (max-width: 850px) {
    width: 42vw;
  }
`
export const LatestNewsTitle = styled.div`
  width: 16vw;
  height: auto;
  font-size: 1.5vw;
  font-weight: 800;
  @media only screen and (max-width: 850px) {
    width: 42vw;
    font-size: 2.5vw;
  }
`
export const NewsAuthor = styled.div`
  font-size: 0.8vw;
  font-weight: 700;
`
export const NewsDate = styled.div``

export const LatestNewsContent = styled.div`
  width: 25vw;
  font-size: 1vw;
  color: #777;
  @media only screen and (max-width: 850px) {
    display: none;
  }
`

export const LatestNewsImgWrapper = styled.div`
  width: 22vw;
  padding-left: 1vw;
`
export const LatestNewsImg = styled.img`
  width: 21vw;
  height: auto;
`
export const LoadMoreWrapper = styled.div`
  width: 68vw;
  border-top: 2px solid #f1f1f1;
  display: flex;
  margin-bottom: 2.5vw;
  align-items: center;
  justify-content: center;
  :hover {
    background-color: #f1f1f1;
    cursor: pointer;
  }
`
export const LoadMoreButton = styled.button`
  border: none;
  font-size: 1.6vw;
  color: #00a562;
  font-weight: 800;
  padding: 1vw 0;
  background: -webkit-linear-gradient(79deg, #00d301, #36c275 50%, #00a562);
  background-clip: border-box;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  :hover {
    cursor: pointer;
  }
`
