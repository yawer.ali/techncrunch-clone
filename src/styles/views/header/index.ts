import styled from 'styled-components'

export const HeaderMainWrapper = styled.div`
  display: flex;
  display: none;
  justify-content: flex-start;
  margin-top: 5px;
  /* border: 2px solid black; */

  @media (max-width: 1024px) {
    display: flex;
  }
`

export const LogoWrapper = styled.div`
  width: 94vw;
`
export const Logo = styled.img`
  width: 8vw;
`
export const MenuBar = styled.div`
  font-size: 2vw;
  margin-top: 10px;
  color: green;
  :hover {
    cursor: pointer;
  }
`
