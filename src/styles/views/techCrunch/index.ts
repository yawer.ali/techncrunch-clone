import styled from 'styled-components'

export const MainNewsWrapper = styled.div`
  margin-left: 15vw;
`

export const TechHeadingWrapper = styled.div`
  margin-top: 1vw;
  display: flex;
  justify-content: space-between;
  padding: 1vw 0;
  width: 67vw;
  border-top: 10px solid;
  border-bottom: 10px solid;
  border-image-slice: 1;
  border-width: 3px;
  border-image-source: linear-gradient(to left, rgba(254, 205, 165) 0%, #fcb900);
`
export const TechCrunchWrapper = styled.div`
  font-size: 1.5vw;
  font-weight: 800;
  background: linear-gradient(79deg, #00d301, #36c275 50%, #00a562);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`
export const Plus = styled.span``
export const HelpingStartupsWrapper = styled.div`
  font-size: 1vw;
  font-weight: 400;
`
export const FaqWrapper = styled.div`
  font-size: 1.5vw;
  font-weight: 800;
`

export const LatestNewsHeading = styled.h2`
  font-size: 2vw;
  font-weight: 800;
  padding-top: 2vw;
`

export const LatestNewsWrapper = styled.div`
  width: 68vw;
  display: flex;
  margin-top: 2vh;
  gap: 4vw;
  padding: 0.5vw 0;
  border-top: 2px solid #f1f1f1;
  :hover {
    cursor: pointer;
    opacity: 0.6;
  }
`

export const LatestNewsTitleAuthDateWrapper = styled.div`
  width: 40vw;
  /* border: 2px solid red; */
  @media only screen and (max-width: 850px) {
    width: 42vw;
  }
`
export const LatestNewsTitle = styled.div`
  width: 30w;
  height: auto;
  font-size: 1.5vw;
  font-weight: 800;
  @media only screen and (max-width: 850px) {
    width: 42vw;
    font-size: 2.5vw;
  }
  /* line-height: 4vh; */
`
export const NewsAuthor = styled.div`
  font-size: 0.8vw;
  font-weight: 700;
`
export const NewsDate = styled.div``

export const LatestNewsContent = styled.div`
  font-size: 0.9vw;
  color: #777;
  @media only screen and (max-width: 850px) {
    display: none;
  }
`

export const LatestNewsImgWrapper = styled.div`
  width: 22vw;
`
export const LatestNewsImg = styled.img`
  max-width: 100%;
  height: auto;
`
export const LoadMoreWrapper = styled.div`
  width: 68vw;
  border-top: 2px solid #f1f1f1;
  display: flex;
  margin-bottom: 2.5vw;
  align-items: center;
  justify-content: center;
  :hover {
    background-color: #f1f1f1;
    cursor: pointer;
  }
`
export const LoadMoreButton = styled.button`
  border: none;
  font-size: 1.6vw;
  color: #00a562;
  font-weight: 800;
  padding: 1vw 0;
  background: -webkit-linear-gradient(79deg, #00d301, #36c275 50%, #00a562);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  :hover {
    cursor: pointer;
  }
`
export const TopNewsMainWrapper = styled.div`
  width: 70vw;
  /* border: 1px solid red; */
  display: flex;
  flex-wrap: wrap;
  padding: 0.5vw 0;
`
export const TopNewsWrapper = styled.div`
  width: 23vw;
  padding: 0.5vw 0;
  /* border: 1px solid green; */
`
export const TopNewsTitle = styled.div`
  width: 20vw;
  font-size: 1vw;
  font-weight: 500;
  color: #333;
  border-bottom: 1px solid #f1f1f1;
`
export const TopNewsImgWrapper = styled.div`
  width: 23vw;
`
export const TopNewsImg = styled.img`
  width: 21vw;
  height: 30vh;
`
