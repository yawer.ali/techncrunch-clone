import styled from 'styled-components'
export const SinglePagePostWrapper = styled.div`
  display: flex;
  margin-left: 15vw;
  width: 60vw;
  /* border: 2px solid red; */
`
export const SinglePostWrapper = styled.div`
  /* border: 2px solid red; */
`
export const SinglePostTitleAndAuthWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const SinglePostTitle = styled.div`
  width: 45vw;
  font-size: 2vw;
  font-weight: 700;
  /* border: 2px solid black; */
`

export const SinglePostAuthWrapper = styled.div`
  width: 45vw;
  display: flex;
  gap: 1vw;
  align-items: center;
  margin-bottom: 1vh;
`

export const SinglePostAuthor = styled.div`
  font-size: 1vw;
  font-weight: 700;
`
export const SinglePostAuthorEmail = styled.div`
  color: #777;
`
export const SinglePostDate = styled.div`
  color: #777;
`

export const SinglePostImgWrapper = styled.div`
  width: 60vw;
  height: 90vh;
  margin-bottom: 1vh;
`

export const SinglePostImg = styled.img`
  width: 60vw;
  height: 90vh;
`

export const SinglePostContent = styled.div`
  font-size: 1.2vw;
  font-weight: 400;
  color: #333;
  /* border: 2px solid blue; */
`
export const ScrollIndicatorWrapper = styled.div`
  /* background-color: red; */
`
