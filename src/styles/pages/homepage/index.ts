import styled from 'styled-components'

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

export const Main = styled.main`
  overflow: hidden;
  display: flex;
`

export const Aside = styled.aside`
  flex: 0 0 12.5vw;
  @media only screen and (max-width: 1024px) {
    display: none;
  }
`

export const Article = styled.article`
  display: flex;
  flex-direction: column;
`
