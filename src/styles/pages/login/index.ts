import styled from 'styled-components'

export const MainWrapper = styled.div`
  display: flex;
`

export const LeftMainWrapper = styled.div`
  width: 65vw;
  height: 100vh;

  @media (max-width: 768px) {
    display: none;
  }
`

export const Img = styled.img`
  width: 65vw;
  height: 100vh;
`

export const RightMainWrapper = styled.div`
  width: 35vw;
  height: 95vh;
  @media (max-width: 1024px) {
    width: 100vw;
  }
`
export const LoginWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  gap: 1.5vw;
  /* border: 2px solid red; */
  height: 95vh;
`
export const LogoWrapper = styled.div`
  width: 4vw;
  height: 4vw;
`
export const Logo = styled.img`
  width: 4vw;
  height: 4vw;
`
export const SignIn = styled.div`
  font-size: 1.2vw;
  font-weight: 600;
`

export const InputAndBtnWrapper = styled.div`
  /* border: 2px solid green; */
  width: 20vw;
`
export const InputWrapper = styled.div``
export const Input = styled.input`
  border: 0;
  border-bottom: 0.1vw solid #b9bdc5;
  width: 20vw;
  margin-bottom: 2vh;
  :focus {
    outline: none;
    border-bottom: 0.1vw solid #00a562;
  }
`

export const NextBtnWrapper = styled.div``
export const Button = styled.button`
  color: #fff;
  font-size: 1vw;
  font-weight: 500;
  background: #00a562;
  border: 1px solid #00a562;
  border-radius: 0.1vw;
  padding: 0.3vw;
  width: 20vw;
  margin-bottom: 2vh;
  :hover {
    background-color: #00c173;
    cursor: pointer;
  }
`

export const SignInAndForgotWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 5vw;
  color: #00a562;
`

export const StaySignedIN = styled.div`
  display: flex;
  :hover {
    cursor: pointer;
  }
`
export const SignInn = styled.div``
export const CheckBoxWrapper = styled.div``
export const InputCheckBox = styled.input`
  width: 1.2vw;
  height: 1.7vh;
`
export const StayedInWrapper = styled.div``
export const ForgottenUserName = styled.div`
  :hover {
    cursor: pointer;
  }
`
export const ForgotUser = styled.div``

export const CreateAccWrapper = styled.div`
  /* border: 2px solid red; */
`
export const CreateAccButton = styled.button`
  color: #00a562;
  font-size: 1vw;
  font-weight: 500;
  background: #fff;
  border: 1px solid #00a562;
  border-radius: 0.1vw;
  padding: 0.3vw;
  width: 20vw;
  :hover {
    cursor: pointer;
  }
`

export const OrContinueWrapper = styled.div`
  font-size: 0.9vw;
`

export const SocialMediaWrapper = styled.div`
  display: flex;
  gap: 1vw;
  width: 20vw;
  /* border: 2px solid red; */
`
export const YahooWrapper = styled.div`
  width: 10vw;
  font-size: 2vw;
  color: #a020f0;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid grey;
  :hover {
    cursor: pointer;
    border: 1px solid #00a562;
  }
`
export const GoogleWrapper = styled.div`
  width: 10vw;
  font-size: 2vw;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid grey;
  :hover {
    cursor: pointer;
    border: 1px solid #00a562;
  }
`

export const TermsPolicyWrapper = styled.div`
  color: green;
  display: flex;
  align-items: center;
  justify-content: center;
`
