import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from 'react-query'
import { LoaderProvider } from 'context/loader'
import HomePage from 'pages/homePage'
import SinglePost from 'components/SinglePost'
import SearchedNews from 'pages/searchNewsPage'
import Login from 'pages/login'
import StartUps from 'views/startUps'
import Venture from 'views/venture'
import Security from 'views/securitySection'
import Crypto from 'views/crypto'
import AppsSection from 'views/appSection'
import Events from 'views/eventSection'
import SingleEvent from 'components/SingleEvent'
import Advertisement from 'views/advertisement'
import Sidebar from 'views/sideBar'
import Header from 'views/header'
import Footer from 'views/footer'
import TechCrunchPhus from 'views/techCrunchPhus'

const queryClient = new QueryClient()

const App = () => {
  return (
    <LoaderProvider>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Header />
          <Sidebar />
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/login" element={<Login />} />
            <Route path="/singlepost" element={<SinglePost />} />
            <Route path="/news" element={<SearchedNews />} />
            <Route path="/techcrunchphus" element={<TechCrunchPhus />} />
            <Route path="/startups" element={<StartUps />} />
            <Route path="/venture" element={<Venture />} />
            <Route path="/security" element={<Security />} />
            <Route path="/crypto" element={<Crypto />} />
            <Route path="/apps" element={<AppsSection />} />
            <Route path="/events" element={<Events />} />
            <Route path="/singlevent" element={<SingleEvent />} />
            <Route path="/advertisement" element={<Advertisement />} />
          </Routes>
          <Footer />
        </BrowserRouter>
      </QueryClientProvider>
    </LoaderProvider>
  )
}

export default App
