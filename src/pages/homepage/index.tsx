import styled from 'styled-components'
// import { Article, Aside, Main, MainContainer } from 'styles/pages/homePage'
import TopNews from 'views/topNewSection'

const HomePage = () => {
  return (
    <MainContainer>
      {/* <Header /> */}
      {/* <Main> */}
      {/* <Aside> */}
      {/* <Sidebar /> */}
      {/* </Aside> */}
      {/* <Article> */}
      <TopNews />

      {/* </Article> */}
      {/* </Main> */}
    </MainContainer>
  )
}

export default HomePage

// styles
export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`
