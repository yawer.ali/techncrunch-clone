import styled from 'styled-components'
import useGet from 'hooks/useGet'
import { useEffect } from 'react'
import Loader from 'components/Loader'
import { useLocation } from 'react-router-dom'
import {
  LatestNewsContent,
  LatestNewsImg,
  LatestNewsImgWrapper,
  LatestNewsTitle,
  LatestNewsTitleAuthDateWrapper,
  LatestNewsWrapper,
  NewsAuthor,
  NewsDate,
} from 'styles/views/topNews'
import moment from 'moment'

const SearchedNews = () => {
  const { state } = useLocation()
  const { refetch: fetchDetails, data, isFetching } = useGet('posts-length', `posts?search=${state}`)
  // console.log(data, 'data from Api')

  useEffect(() => {
    fetchDetails()
  }, [])

  if (isFetching) return <Loader />

  return (
    <MainWrapper>
      <SearchInput type="text" value={state} />
      <SearchButton>Search</SearchButton>
      {data?.map((item: any, index: number) => {
        const AuthorName = 'parsely-author'
        const content = item.excerpt.rendered
        const contentData = content.slice(3, content.length - 15)
        const Title = item.title.rendered

        return (
          <LatestNewsWrapper key={index}>
            <LatestNewsTitleAuthDateWrapper>
              <LatestNewsTitle dangerouslySetInnerHTML={{ __html: Title }}></LatestNewsTitle>
              <NewsAuthor>{item.parselyMeta[AuthorName]}</NewsAuthor>
              <NewsDate>{moment(item.date).format('LLL')}</NewsDate>
            </LatestNewsTitleAuthDateWrapper>
            <LatestNewsContent dangerouslySetInnerHTML={{ __html: contentData }}></LatestNewsContent>
            <LatestNewsImgWrapper>
              <LatestNewsImg src={item.jetpack_featured_media_url} />
            </LatestNewsImgWrapper>
          </LatestNewsWrapper>
        )
      })}
    </MainWrapper>
  )
}

export default SearchedNews

// styles
export const MainWrapper = styled.div`
  margin: 1vw 15vw;
`
export const SearchInput = styled.input``
export const SearchButton = styled.button``
