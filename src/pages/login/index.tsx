import logo from 'assets/images/logo.png'
import {
  Button,
  CheckBoxWrapper,
  CreateAccButton,
  CreateAccWrapper,
  ForgottenUserName,
  ForgotUser,
  GoogleWrapper,
  Img,
  Input,
  InputAndBtnWrapper,
  InputCheckBox,
  InputWrapper,
  LeftMainWrapper,
  LoginWrapper,
  Logo,
  LogoWrapper,
  MainWrapper,
  NextBtnWrapper,
  OrContinueWrapper,
  RightMainWrapper,
  SignIn,
  SignInAndForgotWrapper,
  SignInn,
  SocialMediaWrapper,
  StayedInWrapper,
  StaySignedIN,
  TermsPolicyWrapper,
  YahooWrapper,
} from 'styles/pages/login'
import { FcGoogle } from 'react-icons/fc'
import { FaYahoo } from 'react-icons/fa'
import img from 'assets/images/loginimg.png'

const Login = () => {
  return (
    <MainWrapper>
      <LeftMainWrapper>
        <Img src={img} />
      </LeftMainWrapper>
      <RightMainWrapper>
        <LoginWrapper>
          <LogoWrapper>
            <Logo src={logo} />
          </LogoWrapper>
          <SignIn>Sign In</SignIn>
          <InputAndBtnWrapper>
            <InputWrapper>
              <Input type="email" placeholder="Email Address" />
            </InputWrapper>
            <NextBtnWrapper>
              <Button>Next</Button>
            </NextBtnWrapper>
            <SignInAndForgotWrapper>
              <StaySignedIN>
                <CheckBoxWrapper>
                  <InputCheckBox type="checkbox" />
                </CheckBoxWrapper>
                <StayedInWrapper>
                  <SignInn>Stay signed in</SignInn>
                </StayedInWrapper>
              </StaySignedIN>
              <ForgottenUserName>
                <ForgotUser>Forgotten userName?</ForgotUser>
              </ForgottenUserName>
            </SignInAndForgotWrapper>
          </InputAndBtnWrapper>
          <CreateAccWrapper>
            <CreateAccButton>Create an account</CreateAccButton>
          </CreateAccWrapper>
          <OrContinueWrapper>Or, continue with</OrContinueWrapper>
          <SocialMediaWrapper>
            <YahooWrapper>
              <FaYahoo />
            </YahooWrapper>
            <GoogleWrapper>
              <FcGoogle />
            </GoogleWrapper>
          </SocialMediaWrapper>
        </LoginWrapper>
        <TermsPolicyWrapper>Terms | Privacy</TermsPolicyWrapper>
      </RightMainWrapper>
    </MainWrapper>
  )
}

export default Login
